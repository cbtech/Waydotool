// 
// Simple Server for communication using uinput
// cc -g -Wall waydotoold.c -o waydotoold
//

#include <stdio.h>
#include <linux/uinput.h>
#include <stdlib.h>
#include <unistd.h> // write read
#include <sys/stat.h>	//chmod
#include <fcntl.h>	// O_WRONLY
#include <sys/socket.h>
#include <sys/un.h> 	// sun_family

#include "keycodes.h"
static const char *socket_path = "/tmp/1000-runtime-dir/waydotoold.sock";
static const char *socket_perm = "0777";

void create_uinput_device(int fd) {
	struct uinput_setup usetup;

	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_VIRTUAL,
	usetup.id.vendor = 0x2333,
	usetup.id.product = 0x6666,
	usetup.id.version = 1,
	strcpy(usetup.name, "Waydotool Virtual device");

	if (ioctl(fd,UI_DEV_SETUP,&usetup)) 
		fprintf(stderr,"UI_DEV_SETUP ioctl failed\n");

	if(ioctl(fd,UI_DEV_CREATE)) 
		fprintf(stderr,"UI_DEV_CREATE ioctl failed\n");
}

static void uinput_setup(int fd) {
	static const int event_list[] = { EV_KEY, EV_REL };

	for (int i = 0; i < sizeof(event_list) / sizeof(int); i++) {
		if(ioctl(fd, UI_SET_EVBIT,event_list[i])) 
			fprintf(stderr, "UI_SET_EVBIT %d failed\n", i);
	}
	ioctl(fd,UI_SET_EVBIT,EV_KEY);
	
	for (int i = 0; i < sizeof(key_list) / sizeof(int); i++) {
		if(ioctl(fd, UI_SET_KEYBIT, key_list[i]))
			fprintf(stderr,"UI_SET_KEYBIT %d failed\n",i);
	}

	create_uinput_device(fd);
}

int main(int argc , char *argv[])
{
	int uinput_fd = open("/dev/uinput", O_WRONLY);

	if (uinput_fd < 0) { 
		fprintf(stderr,"Failed to open uinput device");
		abort();
	}

	uinput_setup(uinput_fd);
	
	int fd_so = socket(AF_UNIX, SOCK_DGRAM, 0);
	if( fd_so < 0) {
		fprintf(stderr, "Failed to create socket\n");
		abort();
	}
	
	unlink(socket_path);
	
	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;
	strncpy(socket_addr.sun_path, socket_path, sizeof(socket_addr.sun_path) -1);

	if(bind(fd_so, (struct sockaddr *) &socket_addr,
				sizeof(socket_addr)) < 0) {
		fprintf(stderr,"Unable to bind socket\n");
	}
	
	chmod(socket_path,strtol(socket_perm,NULL,8));
	struct input_event ev;

	while(read(fd_so,&ev,sizeof(struct input_event))) {
		 	write(uinput_fd, &ev,sizeof(ev));
		}
}
