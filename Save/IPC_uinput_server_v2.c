// 
// Simple Server for communication using uinput
// cc -g -Wall IPC_uinput_server.c -o IPC_uinput_server
//

#include <stdio.h>
#include <linux/uinput.h>
#include <stdlib.h>
#include <unistd.h> // write read
#include <sys/stat.h>	//chmod
#include <fcntl.h>	// O_WRONLY
#include <sys/socket.h>
#include <sys/un.h> 	// sun_family

#define NULL 0
static const char *socket_path = "/tmp/1000-runtime-dir/waydotool.sock";
static const char *socket_perm = "0607";

void inspect_list(int* kl)
{
	//for()
}

static void uinput_setup(int fd) {
	static const int event_list[] = { EV_KEY, EV_REL };

	for (int i = 0; i < sizeof(event_list) / sizeof(int); i++)
	{
		if(ioctl(fd, UI_SET_EVBIT,event_list[i])) 
			fprintf(stderr, "UI_SET_EVBIT %d failed\n", i);
	}

	static const int key_list[] = {
	  // 1-10
	  KEY_ESC, KEY_1, KEY_2,KEY_3,KEY_4,KEY_5,KEY_6,KEY_7,KEY_8,
	  // 10-20
	  KEY_9,KEY_0,KEY_KPRIGHTPAREN,KEY_EQUAL,NULL,KEY_TAB,KEY_A,KEY_Z,KEY_E,KEY_R,
	  //20-39
	  KEY_T,KEY_Y,KEY_U,KEY_I,KEY_O,KEY_P,KEY_DOLLAR,KEY_DOLLAR,KEY_ENTER,KEY_LEFTCTRL,
	  KEY_Q,KEY_S,KEY_D,KEY_F,KEY_G,KEY_H,KEY_J,KEY_K,KEY_L,KEY_SEMICOLON,
	  KEY_APOSTROPHE,KEY_GRAVE,KEY_LEFTSHIFT,NULL,KEY_BACKSLASH,KEY_W,KEY_X,KEY_C,KEY_V,KEY_B,
	  //60-69
	  KEY_N,KEY_M,KEY_COMMA,KEY_DOT,KEY_SLASH,KEY_RIGHTSHIFT,KEY_KPASTERISK,KEY_LEFTALT,KEY_SPACE
	       ,KEY_CAPSLOCK,
	  // 70-79
	  KEY_F1,KEY_F2,KEY_F3,KEY_F4,KEY_F5,KEY_F6,KEY_F7,KEY_F8,KEY_F9,KEY_F10,
	  //80-89
	  KEY_NUMLOCK,KEY_SCROLLLOCK,KEY_KP7,KEY_KP8,KEY_KP9,KEY_KPMINUS,KEY_KP4,KEY_KP5,
	  KEY_KP6,
	  //90-99
	  KEY_KPPLUS,KEY_KP1,KEY_KP2,KEY_KP3,KEY_KP0,KEY_KPDOT,NULL,NULL,KEY_F11,KEY_F12,
	  //100-109
	  KEY_RIGHTALT,
	};

	inspect_list(key_list);

	ioctl(fd,UI_SET_EVBIT,EV_KEY);
	
	for (int i = 0; i < sizeof(key_list) / sizeof(int); i++)
	{
		if(ioctl(fd, UI_SET_KEYBIT, key_list[i]))
			fprintf(stderr,"UI_SET_KEYBIT %d failed\n",i);
	}

	struct uinput_setup usetup;

	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_VIRTUAL,
	usetup.id.vendor = 0x2333,
	usetup.id.product = 0x6666,
	usetup.id.version = 1,
	strcpy(usetup.name, "Wdt Virtual device");

	if (ioctl(fd,UI_DEV_SETUP,&usetup)) 
		fprintf(stderr,"UI_DEV_SETUP ioctl failed\n");

	if(ioctl(fd,UI_DEV_CREATE)) 
		fprintf(stderr,"UI_DEV_CREATE ioctl failed\n");
}

int main(int argc , char *argv[])
{
	int fd_ui = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

	if (fd_ui < 0) 
		fprintf(stderr,"Failed to open uinput device");

	uinput_setup(fd_ui);
	
	// SOCK_DGRAM provides connectionless 
	// communication between two systems
	int fd_so = socket(AF_UNIX, SOCK_DGRAM, 0);
	if( fd_so < 0)
		fprintf(stderr, "Failed to create socket\n");
	
	unlink(socket_path);

	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;

	strncpy(socket_addr.sun_path, socket_path, sizeof(socket_addr.sun_path) -1);

	if(bind(fd_so, (const struct sockaddr *) &socket_addr,
				sizeof(socket_addr))) {
		fprintf(stderr,"Unable to bind socket\n");
	}
	
	chmod(socket_path,strtol(socket_perm,NULL,8));
	struct input_event ie;

	while(1) {
		if(recv(fd_so,&ie,sizeof(ie), 0) == sizeof(ie)) {
			write(fd_ui, &ie,sizeof(ie));
			printf("Input event received : value: %d  ,Code:  %d\n", ie.code,ie.value );
		}
	}

	return 0;
}
