//
//    IPC uinput client
// cc -g -Wall -w IPC_uinput_client.c -o IPC_uinput_client
//
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <linux/uinput.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>	// O_WRONLY
#include <sys/un.h>    //sun_family
#include "keycodes.h"

void emit(int fd_socket,int type, int code, int value)
{
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = value;

	write(fd_socket,&ie,sizeof(ie));
}

int ASCII_keycode(int kc)
{
	printf("char %c  %d, KEYCODE %d KEY: %s \n",kc, kc,keycodes[kc].code,keycodes[kc].flag);
	return keycodes[kc].code;
}

void send_char(int fd_socket,char c)
{
	int kc = (int)c;
 	int keycode = ASCII_keycode(kc);	
	
	// Send Shift Key
	if(!strcmp(keycodes[kc].flag,"UPPER")){
	    emit(fd_socket,EV_KEY,0x2a,1);
	}

	if(!strcmp(keycodes[kc].flag,"ALT")) {
		emit(fd_socket,EV_KEY,100,1);
	}
	
	emit(fd_socket,EV_KEY, keycode, 1);
	emit(fd_socket,EV_SYN, SYN_REPORT, 0);
	emit(fd_socket,EV_KEY, keycode, 0);
	emit(fd_socket,EV_SYN, SYN_REPORT, 0);

	if(!strcmp(keycodes[kc].flag,"UPPER")){
		emit(fd_socket,EV_KEY,0x2a,0);
	}

	if(!strcmp(keycodes[kc].flag,"ALT")){
	  emit(fd_socket,EV_KEY,100,0);
	}

}

void send_enter_event(int fd_socket)
{
	// Send Enter Command
	emit(fd_socket,EV_KEY,28,1);
 	emit(fd_socket,EV_KEY,28,0);
}

int main(int argc,char *argv[])
{
	const char *socket_path ="/tmp/1000-runtime-dir/waydotool.sock";

	int fd_so = socket(AF_UNIX,SOCK_DGRAM, 0);

	if(fd_so < 0)
		fprintf(stderr, "Failed to create socket\n");

	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;

	strncpy(socket_addr.sun_path, socket_path,sizeof(socket_addr.sun_path) -1);

	if(connect(fd_so, (const struct sockadd *) &socket_addr, sizeof(socket_addr)))
		fprintf(stderr, "Failled to connect socket\n");

	
	// LOOP TO SEND BUFFER TO SERVER
	if(argc == 2) {
		char *str = malloc(strlen(argv[1] + 1));
		strcpy(str,argv[1]);
	
		for (int i = 0;i < strlen(str); i++){
			send_char(fd_so,str[i]);
		}

		// Press ENTER key
		send_enter_event(fd_so);
	}

	ioctl(fd_so,UI_DEV_DESTROY);
	close(fd_so);
	return 0;
}
