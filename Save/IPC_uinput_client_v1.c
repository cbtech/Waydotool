//
//    IPC uinput client
// cc -g -Wall -w IPC_uinput_client.c -o IPC_uinput_client
//
//
//

#include <stdio.h>
#include <linux/uinput.h>
#include <sys/socket.h>
#include <fcntl.h>	// O_WRONLY
#include <sys/un.h>    //sun_family

void emit(int fd,int type, int code, int value)
{
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = value;

	write(fd,&ie,sizeof(ie));
}

int main(int argc,char *argv[])
{
	const char *socket_path ="/tmp/1000-runtime-dir/waydotool.sock";

	int fd_so = socket(AF_UNIX,SOCK_DGRAM, 0);

	if(fd_so < 0)
		fprintf(stderr, "Failed to create socket\n");

	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;

	strncpy(socket_addr.sun_path, socket_path,sizeof(socket_addr.sun_path) -1);

	if(connect(fd_so, (const struct sockadd *) &socket_addr, sizeof(socket_addr)))
		fprintf(stderr, "Failled to connect socket\n");

	// EMIT EVENT
	int key_test = KEY_A;

	emit(fd_so,EV_KEY, key_test, 1);
	emit(fd_so,EV_SYN, SYN_REPORT, 0);
	emit(fd_so,EV_KEY, key_test, 0);
	emit(fd_so,EV_SYN, SYN_REPORT, 0);
	sleep(1);

	ioctl(fd_so,UI_DEV_DESTROY);
	close(fd_so);
	return 0;
}
