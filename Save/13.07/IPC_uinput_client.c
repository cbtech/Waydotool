//
//    IPC uinput client
// cc -g -Wall -w IPC_uinput_client.c -o IPC_uinput_client
//
//
//

#include <stdio.h>
#include <string.h>
#include <linux/uinput.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>	// O_WRONLY
#include <sys/un.h>    //sun_family
		       //
#include "keycodes.h"

void emit(int fd_socket,int type, int code, int value)
{
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = value;

	write(fd_socket,&ie,sizeof(ie));
}

int ASCII_keycode(char c)
{
	printf("char %c  %d, KEYCODE %d KEY: %s \n",c, c,keycodes[c].code,keycodes[c].flag);
	return keycodes[c].code;
}

void send_char(int fd_socket,char c)
{
	int keycode = 0;
//	printf("Char c: %c \tASCII: %d \tASCII keycode %x \n",c,c,c);

 	keycode = ASCII_keycode(c);	
//	keycode = 0x10;	 // send letter a to server

	if(keycodes[c].flag == "UPPER") {
	    emit(fd_socket,EV_KEY,0x2a,1);
            printf("This Uppercase Key\n");
	}

	// Send a keycode = 0x10;
	emit(fd_socket,EV_KEY, keycode, 1);
	emit(fd_socket,EV_SYN, SYN_REPORT, 0);
	emit(fd_socket,EV_KEY, keycode, 0);
	emit(fd_socket,EV_SYN, SYN_REPORT, 0);
	

	if(keycodes[c].flag == "UPPER") {
	    emit(fd_socket,EV_KEY,0x2a,0);
	}
}

int main(int argc,char *argv[])
{
	const char *socket_path ="/tmp/1000-runtime-dir/waydotool.sock";

	int fd_so = socket(AF_UNIX,SOCK_DGRAM, 0);

	if(fd_so < 0)
		fprintf(stderr, "Failed to create socket\n");

	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;

	strncpy(socket_addr.sun_path, socket_path,sizeof(socket_addr.sun_path) -1);

	if(connect(fd_so, (const struct sockadd *) &socket_addr, sizeof(socket_addr)))
		fprintf(stderr, "Failled to connect socket\n");

	
	// LOOP TO SEND BUFFER TO SERVER
	if(argc == 2) {
		char *str = malloc(strlen(argv[1] + 1));
		strcpy(str,argv[1]);
	
		for (int i = 0;i < strlen(str); i++){
			send_char(fd_so,str[i]);
		}
	}

	ioctl(fd_so,UI_DEV_DESTROY);
	close(fd_so);
	return 0;
}
