//
//    IPC uinput client(waydotool)
// cc -g -Wall waydotool_client.c -o waydotool_client
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <linux/uinput.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>	// O_WRONLY
#include <sys/un.h>    //sun_family
#include <termios.h>
#include <linux/kd.h>
#include "keycodes.h"


void emit(int fd_socket,int type, int code, int value) {
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = value;

	write(fd_socket,&ie,sizeof(ie));
}

int ASCII_keycode(int kc) {
	return keycodes[kc].code;
}

void is_a_special_key(int kc) {
}

void send_char(int fd_socket,char c) {
	int kc = (int)c;
 	int keycode = ASCII_keycode(kc);	
	
	// Send Shift Key
	if(!strcmp(keycodes[kc].flag,"UPPER")){
	    emit(fd_socket,EV_KEY,0x2a,1);
	}

	if(!strcmp(keycodes[kc].flag,"ALT")) {
		emit(fd_socket,EV_KEY,100,1);
	}
	
	emit(fd_socket,EV_KEY, keycode, 1);
	emit(fd_socket,EV_SYN, SYN_REPORT, 0);
	
	usleep(5 * 1000);

	emit(fd_socket,EV_KEY, keycode, 0);
	emit(fd_socket,EV_SYN, SYN_REPORT, 0);

	if(!strcmp(keycodes[kc].flag,"UPPER")){
		emit(fd_socket,EV_KEY,0x2a,0);
	}

	if(!strcmp(keycodes[kc].flag,"ALT")){
	  emit(fd_socket,EV_KEY,100,0);
	}
	usleep(5 * 1000);
}

void init_term() {
	int terminal_descriptor = -1;
	static struct termios terminal_settings;

	if(isatty(STDIN_FILENO))
		terminal_descriptor = STDIN_FILENO;

	tcgetattr(terminal_descriptor, &terminal_settings);
	//Disable cannonical mode | don't Echoing
	terminal_settings.c_lflag &= ~(ICANON | ECHO);
	terminal_settings.c_cc[VTIME] =  1;
	terminal_settings.c_cc[VMIN] = sizeof(256);
	
	// Change immediately
	tcsetattr(terminal_descriptor,TCSANOW,&terminal_settings);

}

void keypress_mode(int socket_desc) {
	init_term();

	int fd = open("/dev/tty0",O_RDWR);
	fd = (isatty(fd) && ioctl(fd,KDGKBTYPE,0));
	int kc = 0;
	printf("FD:: %d\n ",socket_desc);
			
	printf("--- [PRESS MODE] ---\n");
	while(1) {
		printf("KEY> ");
		
		char c = getchar();
		printf("Key: %d Pressed\n" ,ASCII_keycode(c));
		kc = ASCII_keycode(c);
		send_char(fd,kc);
		emit(socket_desc,EV_KEY,kc,1);
		emit(socket_desc,EV_SYN,SYN_REPORT,0);
		emit(socket_desc,EV_KEY,kc,0);
		emit(socket_desc,EV_SYN,SYN_REPORT,0);
	    }
}

void command_mode(int socket_desc) {
	char *str = malloc(1024);
	printf(" \t ---- [ COMMAND mode] ----\n");
	printf("CTRL+c: Quit / CTRL+W: Erase Words / CTRL+U: Undo\n\n");
	while(1) {
		printf("CMD> ");
		fgets(str,1024,stdin);
		
		for(int i=0; i < strlen(str); i++) {
			send_char(socket_desc,str[i]);	
		}
	    }
}

int main(int argc,char *argv[])
{

	const char *socket_path ="/tmp/1000-runtime-dir/waydotoold.sock";

	int socket_desc = socket(AF_UNIX,SOCK_DGRAM, 0);

	if(socket_desc < 0)
		fprintf(stderr, "Failed to create socket\n");

	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;

	strncpy(socket_addr.sun_path, socket_path,sizeof(socket_addr.sun_path) -1);

	if(connect(socket_desc, (const struct sockaddr *) &socket_addr, sizeof(socket_addr)))
		fprintf(stderr, "Failled to connect socket\n");
	
	if(argc < 2) {
		printf("Please enter type [--press / --cmd]\n");
		return 0;
	}

	if(!strcmp(argv[1],"--cmd")) {
		command_mode(socket_desc);
	}

	if(!strcmp(argv[1],"--press")) {
		keypress_mode(socket_desc);
	}

	ioctl(socket_desc,UI_DEV_DESTROY);
	close(socket_desc);
	return 0;
}
